<?php
/**
 * @file
 * velocity_core.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function velocity_core_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'uid' => 0,
  'title' => 'Contact',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => '8de4571b-5d73-442f-833e-87e1d96b6a4a',
  'type' => 'webform',
  'language' => 'und',
  'created' => 1436173604,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '00464219-416e-429f-844d-1c44a1044ba8',
  'revision_uid' => 0,
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'webform' => array(
    'nid' => 1,
    'next_serial' => 2,
    'confirmation' => '',
    'confirmation_format' => NULL,
    'redirect_url' => '<confirmation>',
    'status' => 1,
    'block' => 0,
    'allow_draft' => 0,
    'auto_save' => 0,
    'submit_notice' => 1,
    'confidential' => 0,
    'submit_text' => '',
    'submit_limit' => -1,
    'submit_interval' => -1,
    'total_submit_limit' => -1,
    'total_submit_interval' => -1,
    'progressbar_bar' => 1,
    'progressbar_page_number' => 0,
    'progressbar_percent' => 0,
    'progressbar_pagebreak_labels' => 1,
    'progressbar_include_confirmation' => 1,
    'progressbar_label_first' => 'Start',
    'progressbar_label_confirmation' => 'Complete',
    'preview' => 0,
    'preview_next_button_label' => '',
    'preview_prev_button_label' => '',
    'preview_title' => '',
    'preview_message' => '',
    'preview_message_format' => NULL,
    'preview_excluded_components' => array(),
    'record_exists' => TRUE,
    'roles' => array(
      0 => 1,
      1 => 2,
    ),
    'emails' => array(),
    'components' => array(
      1 => array(
        'nid' => 1,
        'cid' => 1,
        'pid' => 0,
        'form_key' => 'first_name',
        'name' => 'First Name',
        'type' => 'textfield',
        'value' => '',
        'extra' => array(
          'title_display' => 'before',
          'private' => 0,
          'wrapper_classes' => '',
          'css_classes' => '',
          'width' => '',
          'maxlength' => '',
          'field_prefix' => '',
          'field_suffix' => '',
          'disabled' => 0,
          'unique' => 0,
          'description' => '',
          'placeholder' => '',
          'attributes' => array(),
          'analysis' => FALSE,
        ),
        'required' => 1,
        'weight' => 0,
        'page_num' => 1,
      ),
      2 => array(
        'nid' => 1,
        'cid' => 2,
        'pid' => 0,
        'form_key' => 'last_name',
        'name' => 'Last Name',
        'type' => 'textfield',
        'value' => '',
        'extra' => array(
          'title_display' => 'before',
          'private' => 0,
          'wrapper_classes' => '',
          'css_classes' => '',
          'width' => '',
          'maxlength' => '',
          'field_prefix' => '',
          'field_suffix' => '',
          'disabled' => 0,
          'unique' => 0,
          'description' => '',
          'placeholder' => '',
          'attributes' => array(),
          'analysis' => FALSE,
        ),
        'required' => 1,
        'weight' => 1,
        'page_num' => 1,
      ),
      3 => array(
        'nid' => 1,
        'cid' => 3,
        'pid' => 0,
        'form_key' => 'email_address',
        'name' => 'Email Address',
        'type' => 'email',
        'value' => '',
        'extra' => array(
          'title_display' => 'before',
          'private' => 0,
          'wrapper_classes' => '',
          'css_classes' => '',
          'multiple' => 0,
          'format' => 'short',
          'width' => '',
          'unique' => 0,
          'disabled' => 0,
          'description' => '',
          'placeholder' => '',
          'attributes' => array(),
          'analysis' => FALSE,
        ),
        'required' => 1,
        'weight' => 2,
        'page_num' => 1,
      ),
      6 => array(
        'nid' => 1,
        'cid' => 6,
        'pid' => 0,
        'form_key' => 'telephone_number',
        'name' => 'Telephone Number',
        'type' => 'phone',
        'value' => '',
        'extra' => array(
          'country' => 'gb',
          'phone_country_code' => 1,
          'phone_default_country_code' => 1,
          'phone_int_max_length' => 15,
          'title_display' => 'before',
          'private' => 0,
          'wrapper_classes' => '',
          'css_classes' => '',
          'disabled' => 0,
          'width' => '',
          'attributes' => array(),
          'description' => '',
          'placeholder' => '',
          'ca_phone_separator' => '-',
          'ca_phone_parentheses' => 1,
        ),
        'required' => 1,
        'weight' => 3,
        'mandatory' => 0,
        'page_num' => 1,
      ),
      5 => array(
        'nid' => 1,
        'cid' => 5,
        'pid' => 0,
        'form_key' => 'postcode',
        'name' => 'Postcode',
        'type' => 'textfield',
        'value' => '',
        'extra' => array(
          'title_display' => 'before',
          'private' => 0,
          'wrapper_classes' => '',
          'css_classes' => '',
          'width' => '',
          'maxlength' => '',
          'field_prefix' => '',
          'field_suffix' => '',
          'disabled' => 0,
          'unique' => 0,
          'description' => '',
          'placeholder' => '',
          'attributes' => array(),
          'analysis' => FALSE,
        ),
        'required' => 0,
        'weight' => 4,
        'page_num' => 1,
      ),
      4 => array(
        'nid' => 1,
        'cid' => 4,
        'pid' => 0,
        'form_key' => 'message',
        'name' => 'Message',
        'type' => 'textarea',
        'value' => '',
        'extra' => array(
          'title_display' => 'before',
          'private' => 0,
          'wrapper_classes' => '',
          'css_classes' => '',
          'cols' => '',
          'rows' => '',
          'resizable' => 1,
          'disabled' => 0,
          'description' => '',
          'placeholder' => '',
          'attributes' => array(),
          'analysis' => FALSE,
        ),
        'required' => 1,
        'weight' => 5,
        'page_num' => 1,
      ),
    ),
    'conditionals' => array(),
  ),
  'name' => '',
  'picture' => 0,
  'data' => NULL,
  'date' => '2015-07-06 10:06:44 +0100',
);
  $nodes[] = array(
  'uid' => 0,
  'title' => 'Homepage Slider',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => '20b552a3-d9f7-45c6-96f4-35642f55c53f',
  'type' => 'homepage_slider',
  'language' => 'und',
  'created' => 1436350654,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '16eaf25e-447b-4846-b5c2-7e92fb17ac04',
  'revision_uid' => 1,
  'field_homepage_slider_images' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'name' => '',
  'picture' => 0,
  'data' => NULL,
  'date' => '2015-07-08 11:17:34 +0100',
);
  $nodes[] = array(
  'uid' => 0,
  'title' => 'About',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => '615e006a-020f-4403-8a23-e73a6fe21004',
  'type' => 'page',
  'language' => 'und',
  'created' => 1435934877,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '50ce7178-464f-4c16-8b90-476bedc0b227',
  'revision_uid' => 0,
  'body' => array(
    'und' => array(
      0 => array(
        'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris ullamcorper tincidunt nunc, vitae lacinia eros tristique et. Phasellus pulvinar eu ex ut semper. Aliquam condimentum leo et neque posuere rutrum. Quisque auctor eros vel convallis dictum. Nam at ultricies tellus. Aliquam orci mi, varius ut ultricies in, euismod at turpis. Ut ornare tincidunt risus, id posuere mauris. Quisque tristique ex mi, sed viverra augue cursus a.

Ut tellus erat, ultrices eu augue vitae, fermentum placerat augue. Fusce elementum lorem at suscipit aliquam. Vivamus venenatis purus vel leo pretium fringilla. Donec ac est id nibh fringilla suscipit. Vestibulum pretium congue bibendum. Duis eget nibh purus. Ut eu vulputate nulla. Sed vehicula ipsum sed mauris ultricies, at eleifend ex tempor. Integer laoreet tellus sem. Phasellus ac nibh convallis, ornare felis fringilla, efficitur arcu. Nam facilisis tellus vitae sollicitudin rutrum. Aenean fermentum metus eu sem efficitur, vitae semper turpis condimentum. Sed egestas ipsum eros, quis iaculis est porttitor scelerisque. In dolor leo, consectetur ac scelerisque id, elementum ut nisi. Mauris a leo sed elit hendrerit dictum.

Fusce mattis feugiat ullamcorper. Donec ultricies scelerisque mollis. Praesent non consectetur massa. Sed viverra nulla est, nec gravida nibh semper vel. Vivamus sit amet scelerisque ex, efficitur pretium elit. Mauris convallis nisi nec euismod placerat. Fusce lectus erat, sollicitudin eu elit ut, efficitur molestie mauris. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum lacus libero, lacinia a erat a, semper luctus tortor. Duis eget massa iaculis, rutrum nulla nec, rutrum tellus. Donec vulputate massa in est ullamcorper, quis dictum est dapibus. Nullam quis dolor et tellus consectetur porta in luctus magna.

Quisque hendrerit risus ut turpis commodo, in pharetra nisl blandit. Maecenas magna augue, aliquet a ornare sed, suscipit sed libero. In in ex in purus fringilla interdum non nec sem. Integer ornare mi sit amet mauris condimentum maximus. Curabitur suscipit condimentum lorem, quis tempor elit ornare et. Proin tincidunt, nisl non cursus porttitor, lacus elit vulputate mauris, nec sodales purus ante id ante. Donec ullamcorper, nunc in porta fringilla, quam libero tempor ipsum, nec aliquet elit eros vel nulla. Suspendisse scelerisque ante sed molestie venenatis. Proin ut tempus elit, a ultrices urna. Sed elit erat, lacinia ut leo sit amet, consectetur pharetra lorem. Donec lectus purus, facilisis vitae turpis vel, porttitor bibendum nisi. Nullam ac ante id leo mollis lacinia. Vivamus facilisis, tortor in consectetur ullamcorper, turpis felis ultricies est, consequat lacinia nibh enim sed quam. Nullam scelerisque massa id elit volutpat, consequat lobortis nisl accumsan. Donec faucibus nisl id enim rhoncus, sit amet egestas nulla feugiat. Etiam non urna auctor, tincidunt enim aliquet, ultricies eros.

Etiam fermentum finibus augue et congue. Sed maximus ante sed felis fermentum, in ultrices eros lacinia. Cras bibendum malesuada porta. Fusce sagittis nibh porta maximus maximus. Vivamus quis arcu non neque volutpat vestibulum nec quis sapien. Phasellus sed tempus nisi. Curabitur urna dolor, porta et laoreet vitae, mollis a nunc.',
        'summary' => '',
        'format' => 'filtered_html',
        'safe_value' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris ullamcorper tincidunt nunc, vitae lacinia eros tristique et. Phasellus pulvinar eu ex ut semper. Aliquam condimentum leo et neque posuere rutrum. Quisque auctor eros vel convallis dictum. Nam at ultricies tellus. Aliquam orci mi, varius ut ultricies in, euismod at turpis. Ut ornare tincidunt risus, id posuere mauris. Quisque tristique ex mi, sed viverra augue cursus a.</p>
<p>Ut tellus erat, ultrices eu augue vitae, fermentum placerat augue. Fusce elementum lorem at suscipit aliquam. Vivamus venenatis purus vel leo pretium fringilla. Donec ac est id nibh fringilla suscipit. Vestibulum pretium congue bibendum. Duis eget nibh purus. Ut eu vulputate nulla. Sed vehicula ipsum sed mauris ultricies, at eleifend ex tempor. Integer laoreet tellus sem. Phasellus ac nibh convallis, ornare felis fringilla, efficitur arcu. Nam facilisis tellus vitae sollicitudin rutrum. Aenean fermentum metus eu sem efficitur, vitae semper turpis condimentum. Sed egestas ipsum eros, quis iaculis est porttitor scelerisque. In dolor leo, consectetur ac scelerisque id, elementum ut nisi. Mauris a leo sed elit hendrerit dictum.</p>
<p>Fusce mattis feugiat ullamcorper. Donec ultricies scelerisque mollis. Praesent non consectetur massa. Sed viverra nulla est, nec gravida nibh semper vel. Vivamus sit amet scelerisque ex, efficitur pretium elit. Mauris convallis nisi nec euismod placerat. Fusce lectus erat, sollicitudin eu elit ut, efficitur molestie mauris. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum lacus libero, lacinia a erat a, semper luctus tortor. Duis eget massa iaculis, rutrum nulla nec, rutrum tellus. Donec vulputate massa in est ullamcorper, quis dictum est dapibus. Nullam quis dolor et tellus consectetur porta in luctus magna.</p>
<p>Quisque hendrerit risus ut turpis commodo, in pharetra nisl blandit. Maecenas magna augue, aliquet a ornare sed, suscipit sed libero. In in ex in purus fringilla interdum non nec sem. Integer ornare mi sit amet mauris condimentum maximus. Curabitur suscipit condimentum lorem, quis tempor elit ornare et. Proin tincidunt, nisl non cursus porttitor, lacus elit vulputate mauris, nec sodales purus ante id ante. Donec ullamcorper, nunc in porta fringilla, quam libero tempor ipsum, nec aliquet elit eros vel nulla. Suspendisse scelerisque ante sed molestie venenatis. Proin ut tempus elit, a ultrices urna. Sed elit erat, lacinia ut leo sit amet, consectetur pharetra lorem. Donec lectus purus, facilisis vitae turpis vel, porttitor bibendum nisi. Nullam ac ante id leo mollis lacinia. Vivamus facilisis, tortor in consectetur ullamcorper, turpis felis ultricies est, consequat lacinia nibh enim sed quam. Nullam scelerisque massa id elit volutpat, consequat lobortis nisl accumsan. Donec faucibus nisl id enim rhoncus, sit amet egestas nulla feugiat. Etiam non urna auctor, tincidunt enim aliquet, ultricies eros.</p>
<p>Etiam fermentum finibus augue et congue. Sed maximus ante sed felis fermentum, in ultrices eros lacinia. Cras bibendum malesuada porta. Fusce sagittis nibh porta maximus maximus. Vivamus quis arcu non neque volutpat vestibulum nec quis sapien. Phasellus sed tempus nisi. Curabitur urna dolor, porta et laoreet vitae, mollis a nunc.</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_basic_page_images' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'name' => '',
  'picture' => 0,
  'data' => NULL,
  'date' => '2015-07-03 15:47:57 +0100',
);
  return $nodes;
}
