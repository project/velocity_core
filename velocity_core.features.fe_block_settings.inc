<?php
/**
 * @file
 * velocity_core.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function velocity_core_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['addthis-addthis_block'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'addthis_block',
    'module' => 'addthis',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'velocity_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'velocity_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['block-contact_us'] = array(
    'cache' => -1,
    'css_class' => 'contact-us-block',
    'custom' => 0,
    'machine_name' => 'contact_us',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'velocity_theme' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'velocity_theme',
        'weight' => -9,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['block-social_links'] = array(
    'cache' => -1,
    'css_class' => 'social-links-block',
    'custom' => 0,
    'machine_name' => 'social_links',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'velocity_theme' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'velocity_theme',
        'weight' => -10,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['copyright_block-copyright_block'] = array(
    'cache' => 8,
    'css_class' => 'copyright-block',
    'custom' => 0,
    'delta' => 'copyright_block',
    'module' => 'copyright_block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'velocity_theme' => array(
        'region' => 'copyright',
        'status' => 1,
        'theme' => 'velocity_theme',
        'weight' => -9,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['masquerade-masquerade'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'masquerade',
    'module' => 'masquerade',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'velocity_theme' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'velocity_theme',
        'weight' => -8,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-main-menu'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'main-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'velocity_theme' => array(
        'region' => 'nav',
        'status' => 1,
        'theme' => 'velocity_theme',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  return $export;
}
