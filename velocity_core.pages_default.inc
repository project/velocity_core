<?php
/**
 * @file
 * velocity_core.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function velocity_core_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view__basic_page';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -28;
  $handler->conf = array(
    'title' => 'Basic Page',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'basic-page',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'basic_page',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'page' => 'page',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'edda0ece-9386-49ea-8fa5-66025caef0bc';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-2b3b989c-9847-467a-90b8-d9782794dd5d';
    $pane->panel = 'left';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_basic_page_images';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'image',
      'delta_limit' => '0',
      'delta_offset' => '0',
      'delta_reversed' => 0,
      'formatter_settings' => array(
        'image_style' => 'medium',
        'image_link' => '',
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2b3b989c-9847-467a-90b8-d9782794dd5d';
    $display->content['new-2b3b989c-9847-467a-90b8-d9782794dd5d'] = $pane;
    $display->panels['left'][0] = 'new-2b3b989c-9847-467a-90b8-d9782794dd5d';
    $pane = new stdClass();
    $pane->pid = 'new-2502b784-1bcb-4bbd-8b8b-0a77aeb81607';
    $pane->panel = 'right';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2502b784-1bcb-4bbd-8b8b-0a77aeb81607';
    $display->content['new-2502b784-1bcb-4bbd-8b8b-0a77aeb81607'] = $pane;
    $display->panels['right'][0] = 'new-2502b784-1bcb-4bbd-8b8b-0a77aeb81607';
    $pane = new stdClass();
    $pane->pid = 'new-8bf2ba58-0ee3-4ab8-9341-d53a7e5797dd';
    $pane->panel = 'top';
    $pane->type = 'page_title';
    $pane->subtype = 'page_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'markup' => 'h1',
      'class' => '',
      'id' => '',
      'override_title' => 1,
      'override_title_text' => '%node:title',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '8bf2ba58-0ee3-4ab8-9341-d53a7e5797dd';
    $display->content['new-8bf2ba58-0ee3-4ab8-9341-d53a7e5797dd'] = $pane;
    $display->panels['top'][0] = 'new-8bf2ba58-0ee3-4ab8-9341-d53a7e5797dd';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-8bf2ba58-0ee3-4ab8-9341-d53a7e5797dd';
  $handler->conf['display'] = $display;
  $export['node_view__basic_page'] = $handler;

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view__blog_article';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -29;
  $handler->conf = array(
    'title' => 'Blog Article',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'blog-article',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'blog_article',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'blog_article' => 'blog_article',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '8ce6f2a4-49e9-4dd4-a88f-46cc86a3d52a';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-bffaa19f-5e94-4a06-b1d3-45feff118033';
    $pane->panel = 'left';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_blog_article_images';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'image',
      'delta_limit' => '0',
      'delta_offset' => '0',
      'delta_reversed' => 0,
      'formatter_settings' => array(
        'image_style' => 'medium',
        'image_link' => '',
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'bffaa19f-5e94-4a06-b1d3-45feff118033';
    $display->content['new-bffaa19f-5e94-4a06-b1d3-45feff118033'] = $pane;
    $display->panels['left'][0] = 'new-bffaa19f-5e94-4a06-b1d3-45feff118033';
    $pane = new stdClass();
    $pane->pid = 'new-2b9491f0-1e83-4aac-b226-00fbe6fc49eb';
    $pane->panel = 'right';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2b9491f0-1e83-4aac-b226-00fbe6fc49eb';
    $display->content['new-2b9491f0-1e83-4aac-b226-00fbe6fc49eb'] = $pane;
    $display->panels['right'][0] = 'new-2b9491f0-1e83-4aac-b226-00fbe6fc49eb';
    $pane = new stdClass();
    $pane->pid = 'new-7719f08b-10c2-426f-aa6c-678ba9e73759';
    $pane->panel = 'right';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_blog_article_tags';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'above',
      'formatter' => 'taxonomy_term_reference_plain',
      'delta_limit' => '0',
      'delta_offset' => '0',
      'delta_reversed' => 0,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '7719f08b-10c2-426f-aa6c-678ba9e73759';
    $display->content['new-7719f08b-10c2-426f-aa6c-678ba9e73759'] = $pane;
    $display->panels['right'][1] = 'new-7719f08b-10c2-426f-aa6c-678ba9e73759';
    $pane = new stdClass();
    $pane->pid = 'new-1fa30ab3-da4b-4a1a-aaff-bad584a2c4c2';
    $pane->panel = 'right';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_blog_article_share';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'addthis_basic_toolbox',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'share_services' => 'facebook,twitter,linkedin,google_plusone_share,pinterest_share,email,print',
        'buttons_size' => 'addthis_32x32_style',
        'counter_orientation' => 'vertical',
        'extra_css' => '',
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '1fa30ab3-da4b-4a1a-aaff-bad584a2c4c2';
    $display->content['new-1fa30ab3-da4b-4a1a-aaff-bad584a2c4c2'] = $pane;
    $display->panels['right'][2] = 'new-1fa30ab3-da4b-4a1a-aaff-bad584a2c4c2';
    $pane = new stdClass();
    $pane->pid = 'new-16ab2bda-2cc6-45ef-97be-12775ea411f0';
    $pane->panel = 'top';
    $pane->type = 'page_title';
    $pane->subtype = 'page_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'markup' => 'h1',
      'class' => '',
      'id' => '',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '16ab2bda-2cc6-45ef-97be-12775ea411f0';
    $display->content['new-16ab2bda-2cc6-45ef-97be-12775ea411f0'] = $pane;
    $display->panels['top'][0] = 'new-16ab2bda-2cc6-45ef-97be-12775ea411f0';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-16ab2bda-2cc6-45ef-97be-12775ea411f0';
  $handler->conf['display'] = $display;
  $export['node_view__blog_article'] = $handler;

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view__webform';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -30;
  $handler->conf = array(
    'title' => 'Webform',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => 'webform',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'webform',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'webform' => 'webform',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '152c4c50-e7a0-4fc4-9d25-0d8cdca050fe';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-cff75a69-4478-4c2c-896e-026c0115dbc4';
    $pane->panel = 'middle';
    $pane->type = 'page_title';
    $pane->subtype = 'page_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'markup' => 'h1',
      'class' => '',
      'id' => '',
      'override_title' => 1,
      'override_title_text' => '%node:title',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'cff75a69-4478-4c2c-896e-026c0115dbc4';
    $display->content['new-cff75a69-4478-4c2c-896e-026c0115dbc4'] = $pane;
    $display->panels['middle'][0] = 'new-cff75a69-4478-4c2c-896e-026c0115dbc4';
    $pane = new stdClass();
    $pane->pid = 'new-5917464e-c54a-433a-b2e3-54cdc26d6d42';
    $pane->panel = 'middle';
    $pane->type = 'entity_field_extra';
    $pane->subtype = 'node:webform';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '5917464e-c54a-433a-b2e3-54cdc26d6d42';
    $display->content['new-5917464e-c54a-433a-b2e3-54cdc26d6d42'] = $pane;
    $display->panels['middle'][1] = 'new-5917464e-c54a-433a-b2e3-54cdc26d6d42';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-5917464e-c54a-433a-b2e3-54cdc26d6d42';
  $handler->conf['display'] = $display;
  $export['node_view__webform'] = $handler;

  return $export;
}

/**
 * Implements hook_default_page_manager_pages().
 */
function velocity_core_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'blog';
  $page->task = 'page';
  $page->admin_title = 'Blog';
  $page->admin_description = '';
  $page->path = 'blog';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Blog',
    'name' => 'main-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_blog__panel_context_650c2135-8138-459a-8690-d248fc9e6e51';
  $handler->task = 'page';
  $handler->subtask = 'blog';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => 'blog',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '35d07a7a-7c09-4dc9-9d24-4751c71ffc9f';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-5d819277-e77c-4e84-aa6e-c2563c17afd5';
    $pane->panel = 'middle';
    $pane->type = 'page_title';
    $pane->subtype = 'page_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'markup' => 'h1',
      'class' => '',
      'id' => '',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5d819277-e77c-4e84-aa6e-c2563c17afd5';
    $display->content['new-5d819277-e77c-4e84-aa6e-c2563c17afd5'] = $pane;
    $display->panels['middle'][0] = 'new-5d819277-e77c-4e84-aa6e-c2563c17afd5';
    $pane = new stdClass();
    $pane->pid = 'new-4afbc121-bd44-4862-9a42-667ba1f458b8';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'blog_articles-master_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '4afbc121-bd44-4862-9a42-667ba1f458b8';
    $display->content['new-4afbc121-bd44-4862-9a42-667ba1f458b8'] = $pane;
    $display->panels['middle'][1] = 'new-4afbc121-bd44-4862-9a42-667ba1f458b8';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-5d819277-e77c-4e84-aa6e-c2563c17afd5';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['blog'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'gallery';
  $page->task = 'page';
  $page->admin_title = 'Gallery';
  $page->admin_description = '';
  $page->path = 'gallery';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Gallery',
    'name' => 'main-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_gallery__panel_context_e3d2686c-504f-48e2-a083-95a46c1e2c6e';
  $handler->task = 'page';
  $handler->subtask = 'gallery';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => 'gallery',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Gallery';
  $display->uuid = '7dac54ff-9d52-4b6a-8a65-5480c1b985fc';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-420d8d7d-96d8-46a2-be88-44ae2fb21cf4';
    $pane->panel = 'middle';
    $pane->type = 'page_title';
    $pane->subtype = 'page_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'markup' => 'h1',
      'class' => '',
      'id' => '',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '420d8d7d-96d8-46a2-be88-44ae2fb21cf4';
    $display->content['new-420d8d7d-96d8-46a2-be88-44ae2fb21cf4'] = $pane;
    $display->panels['middle'][0] = 'new-420d8d7d-96d8-46a2-be88-44ae2fb21cf4';
    $pane = new stdClass();
    $pane->pid = 'new-46f91235-1fad-4fa3-991c-aa9e412ad2b7';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'gallery-master_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '46f91235-1fad-4fa3-991c-aa9e412ad2b7';
    $display->content['new-46f91235-1fad-4fa3-991c-aa9e412ad2b7'] = $pane;
    $display->panels['middle'][1] = 'new-46f91235-1fad-4fa3-991c-aa9e412ad2b7';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['gallery'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'home_page';
  $page->task = 'page';
  $page->admin_title = 'Home Page';
  $page->admin_description = '';
  $page->path = 'home';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_home_page__panel_context_24357e98-051a-492b-a75b-8e268c790c2e';
  $handler->task = 'page';
  $handler->subtask = 'home_page';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => 'homepage',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'c3e6cdfa-3ff5-4923-8186-a488af8d47f5';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-73c75f22-3c82-48bc-a59c-0a2bd53cf271';
    $pane->panel = 'left';
    $pane->type = 'views_panes';
    $pane->subtype = 'blog_articles-homepage_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '73c75f22-3c82-48bc-a59c-0a2bd53cf271';
    $display->content['new-73c75f22-3c82-48bc-a59c-0a2bd53cf271'] = $pane;
    $display->panels['left'][0] = 'new-73c75f22-3c82-48bc-a59c-0a2bd53cf271';
    $pane = new stdClass();
    $pane->pid = 'new-045e5b6b-7e6d-49fb-b934-8c6e83616afd';
    $pane->panel = 'right';
    $pane->type = 'views_panes';
    $pane->subtype = 'gallery-homepage_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '045e5b6b-7e6d-49fb-b934-8c6e83616afd';
    $display->content['new-045e5b6b-7e6d-49fb-b934-8c6e83616afd'] = $pane;
    $display->panels['right'][0] = 'new-045e5b6b-7e6d-49fb-b934-8c6e83616afd';
    $pane = new stdClass();
    $pane->pid = 'new-ddfc28bd-a094-4ec0-b43e-999e84c102e4';
    $pane->panel = 'top';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Core Message and CTA',
      'title' => '',
      'body' => '<div class="core-message">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean elementum consectetur pharetra. Vivamus vel risus gravida ligula vehicula venenatis. Maecenas porttitor ac mauris et accumsan. Proin arcu nisi, congue viverra.</div>

<div class="cta"><a href="/contact" class="button">Contact us</a> today to discuss your requirements.</div>',
      'format' => 'full_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'statement-with-cta',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'ddfc28bd-a094-4ec0-b43e-999e84c102e4';
    $display->content['new-ddfc28bd-a094-4ec0-b43e-999e84c102e4'] = $pane;
    $display->panels['top'][0] = 'new-ddfc28bd-a094-4ec0-b43e-999e84c102e4';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['home_page'] = $page;

  return $pages;

}
