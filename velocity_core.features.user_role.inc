<?php
/**
 * @file
 * velocity_core.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function velocity_core_user_default_roles() {
  $roles = array();

  // Exported role: content editor.
  $roles['content editor'] = array(
    'name' => 'content editor',
    'weight' => 4,
  );

  // Exported role: editor.
  $roles['editor'] = array(
    'name' => 'editor',
    'weight' => 3,
  );

  return $roles;
}
