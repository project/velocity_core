<?php
/**
 * @file
 * velocity_core.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function velocity_core_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Contact Us Block';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'contact_us';
  $fe_block_boxes->body = '<div class="company-name"><h3>Company Name</h3></div>
<div class="company-address">
<p>Address Line 1</p>
<p>Address Line 2</p>
<p>Town/City</p>
<p>County</p>
<p>Postcode</p>
<p>Country</p>
</div>
<div class="company-contact">
<a href="tel:00442000000000">020 00 00 00 00</a>
<a href="mailto:blah@companyname.co.uk">blah@companyname.co.uk</a>
</div>

';

  $export['contact_us'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Social Links Block';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'social_links';
  $fe_block_boxes->body = '<div class="social-title"><h3>Connect With Us</h3></div>
<div class="social-links">
<div class="facebook"><a href="www.facebook.com/">Facebook</a></div>
<div class="twitter"><a href="www.twitter.com/">Twitter</a></div>
<div class="linkedin"><a href="www.linkedin.com/">LinkedIn</a></div><br />
<div class="googleplus"><a href="www.plus.google.com/">Google+</a></div>
<div class="youtube"><a href="www.youtube.com/">YouTube</a></div>
</div>
';

  $export['social_links'] = $fe_block_boxes;

  return $export;
}
